var AkkApp = angular.module('AkkApp', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate']);

AkkApp.config(function($routeProvider) {

    /* Add New Routes Above */
    $routeProvider.otherwise({redirectTo:'/'});

});

AkkApp.run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});

AkkApp.controller('AccCtrl', ['$scope','$rootScope', function ($scope,$rootScope) {
   
    $rootScope.closeTabs = function() {
     
        angular.forEach($rootScope.scope_array, function(value, key){

          if(!value.counter)
          {
            value.slideUp();  
          }
          
        });
    };
    $rootScope.scope_array = [];
    $rootScope.indexer = 0;

 

}]);

AkkApp.directive('accordButton', ['$rootScope',function factory($rootScope) {
  var directiveDefinitionObject = {
    template : function(tElement, tAttrs) {
        var html_string =  '<div class="col-md-3 accord-button no-gutter"><div>' + 
                               '<img  class="img-responsive" src="' + tAttrs.photoSrc + '" >' + 
                                   '<div tabindex={{button_index}} class="image-overlay" ng-click="update_grid()" >' +
                                    '<div class="image-title">' + tAttrs.title +'</div>' +
                                    '<div class="image-subtitle">' + tAttrs.subtitle +'</div>'  +
                                   '</div>' + 
                                '</img>' +
                            '</div>' + 
                            '<div class="col-xs-12 visible-xs tab-info-bar" ng-transclude></div></div>' ;
        return html_string;
    },
    replace:true,
    transclude: true,
    restrict: 'EA',
    require:['^accordContainer','^accordRow'],
    scope: {},
    controller: function($scope,$element,$attrs,$transclude,$timeout) { 

         var iScope = angular.element($element).isolateScope();
         $rootScope.scope_array[$rootScope.indexer] = iScope;

         $rootScope.indexer = $rootScope.indexer + 1;
         $scope.button_index = $rootScope.indexer;
         $scope.counter = true;

         $scope.slideDown = function(){
             angular.element($element).find('.tab-info-bar').css({'height':'90px' , 'background':'#524b6b'});
             $timeout(function(){
                $rootScope.closeTabs();
                $scope.counter = false ;
             }, 300);

         }; 

         $scope.slideUp = function(){
             $scope.counter = true;
             angular.element($element).find('.tab-info-bar').css({'height':'0px' , 'background':'#524b6b'});
         }; 
        

     },
    compile: function compile(tElement, tAttrs, transclude) {
      return {
        pre: function preLink(scope, iElement, iAttrs, controller) {  },
        post: function postLink(scope, iElement, iAttrs, controller,transclude) { 
            
            var infobarId = controller[1].getInfobarId();
            var infobar_content = "";

            transclude(scope, function(clone) {
               infobar_content = clone.context.innerHTML;
            });

            scope.update_grid = function() {
               if(scope.counter)
                {
                   scope.slideDown();  
                   controller[0].slideDownInfobar('#'+infobarId);
                   controller[1].setcontent(infobar_content);
                   controller[0].setTitle('#'+infobarId,iAttrs.title,iAttrs.subtitle);
                   controller[0].setPhoto('#'+infobarId,iAttrs.photoSrc);
                }
                else
                {
                   scope.slideUp();  
                   controller[0].slideUpInfobar('#'+infobarId);
                }  
            };
          }
       };
    },
 };
  return directiveDefinitionObject;
}]);

AkkApp.directive('accordRow', [function factory() {
  var directiveDefinitionObject = {
    template: '<div class="row accord-row"  ng-transclude></div>', // or // function(tElement, tAttrs) { ... },
    transclude: true,
    restrict: 'EA',
    controller: function($scope, $element, $attrs, $transclude) { 
         $scope.counter = false;
      
         this.slideDown = function(){
             angular.element($element).css({'height':'45px' , 'background':'#524b6b'});
         }; 
         this.slideUp = function(){
             angular.element($element).css({'height':'auto' , 'background':'#524b6b'});
         }; 

         this.getInfobarId = function(){
             return $attrs.infobarId;
         };

         this.setcontent = function(content) {
            $scope[$attrs.content] = content;
         };
     },
    compile: function compile(tElement, tAttrs, transclude) {
      return {
        pre: function preLink(scope, iElement, iAttrs, controller) {  },
        post: function postLink(scope, iElement, iAttrs, controller) { 
            
         }
      };

    },
    
  };
  return directiveDefinitionObject;
}]);


AkkApp.directive('accordContainer', ['$timeout',function factory($timeout) {
  var directiveDefinitionObject = {
    template: '<div ng-transclude></div>', // or // function(tElement, tAttrs) { ... },
    replace:true,
    transclude: true,
    restrict: 'EA',
    templateNamespace: 'html',
    controller: function($scope, $element, $attrs, $transclude) {

      var infobar_hash = {};
      $scope.photo_title = "title";
      $scope.photo_sub_title = "";

      this.slideDownInfobar = function(id){
         infobar_hash[id].slideDown();
      };

      this.slideUpInfobar = function(id){
          infobar_hash[id].slideUp();
      };


      this.closeInfobars = function(excludeScope) {
          angular.forEach(infobar_hash, function(value, key){
            if(!value.counter && (excludeScope !== value))
            {
              value.slideUp();  
            }
       
          });
      };

      this.addinfobar = function(id,scope){
          infobar_hash['#'+id] = scope;
      };

      this.setTitle= function(id,title_string,subtitle_string) {
          infobar_hash[id].setPhotoTitle(title_string,subtitle_string);
      };

      this.setPhoto = function(id,path){
          infobar_hash[id].setPhoto(path);
      };

    },
    compile: function compile(tElement, tAttrs, transclude) {
      return {
        pre: function preLink(scope, iElement, iAttrs, controller) {  },
        post: function postLink(scope, iElement, iAttrs, controller) { }
      };
     },
   };
  return directiveDefinitionObject;
}]);


AkkApp.directive('infobar', ['$timeout',function factory($timeout) {
  var directiveDefinitionObject = {
    template: function(tElement, tAttrs) {
       var title = (tAttrs.title !== "" && tAttrs.title ) ? tAttrs.title : "title_1";
       var html_string = '<div class="row hidden-xs hidden-sm visible-md visible-lg info-bar">' + 
                            '<div class="col-md-12 full-height">' + 
                               '<div class="infobar-section">' + 
                                  '<div class="container-fluid no-gutter full-height">' +
                                     '<div class="row-fluid full-height infobar-transition">' +  
                                       '<div class="col-md-8 no-gutter full-height">' +                                     
                                           '<div class="h2 info-title" >{{photo_title}}</div>' + 
                                           '<div class="info-content" ng-transclude></div>' +
                                       '</div>' + 
                                       '<div class="col-md-4 img-content">' +
                                            '<img class="image-class" ng-src={{photo_src}} >' + 
                                       '</div>' +
                                     '</div>' + 
                                   '</div>' +  
                               '</div>' + 
                             '</div>' + 
                           '</div>';
       return html_string;
     },
    replace:true,
    transclude: true,
    restrict: 'EA',
    templateNamespace: 'html',
    scope: {},
    require:'^accordContainer',
    controller: function($scope, $element, $attrs, $transclude) { 

    $scope.counter = true;
    $scope.photo_title = "";
    $scope.photo_subtitle = "";
    $scope.photo_src = "";

    $scope.setPhotoTitle = function(title_string,subtitle_string) {
        angular.element($element).find('.row-fluid').addClass("animated fadeIn");

        $scope.photo_title = title_string;
        $scope.photo_subtitle = subtitle_string;        

        angular.element($element).find('.row-fluid').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            angular.element($element).find('.row-fluid').removeClass("animated fadeIn");
        });

    };

    $scope.setPhoto = function(photo_path) {
          $scope.photo_src = photo_path;
    };

     },
    

    compile: function compile(tElement, tAttrs, transclude) {
      return {
        pre: function preLink(scope, iElement, iAttrs, controller) {  },
        post: function postLink(scope, iElement, iAttrs, controller) {
         
            var iScope = angular.element(iElement).isolateScope();
            controller.addinfobar(iAttrs.id,iScope);

            scope.slideDown = function(){

                angular.element(iElement).css({'height':'250px'});

                $timeout(function(){
                  controller.closeInfobars(scope);
                  scope.counter = false ;
                }, 300);
            }; 

            scope.slideUp = function(){
                 scope.counter = true;
                 angular.element(iElement).css({'height':'0px'});
            }; 
          }
        };
      },
    };
  return directiveDefinitionObject;
}]);


